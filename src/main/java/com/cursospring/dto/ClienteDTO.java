package com.cursospring.dto;

import com.cursospring.domain.Cliente;
import com.cursospring.domain.Endereco;
import com.cursospring.services.validation.ClienteUpdate;
import org.apache.catalina.mapper.Mapper;
import org.hibernate.validator.constraints.Length;
import org.modelmapper.ModelMapper;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@ClienteUpdate
public class ClienteDTO implements Serializable {

    private Integer id;
    @NotEmpty(message = "Preenchimento obrigatorio")
    @Length(message = "Minimo  5, Maximo 120",min = 5,max = 120)
    private String nome;

    @NotEmpty(message = "Preenchimento Email obrigatorio")
    @Email(message = "Email Invalido")
    private String email;

    public ClienteDTO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public static Cliente convertToCliente(ClienteDTO clienteDTO){
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(clienteDTO,Cliente.class);

    }

    public static ClienteDTO convertToClienteDTO(Cliente item) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(item,ClienteDTO.class);
    }
}
