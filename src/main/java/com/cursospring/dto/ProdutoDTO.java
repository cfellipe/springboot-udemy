package com.cursospring.dto;

import com.cursospring.domain.Categoria;
import com.cursospring.domain.Produto;
import org.modelmapper.ModelMapper;

public class ProdutoDTO {

    private Integer id;
    private String nome;
    private Double preco;

    public ProdutoDTO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getVPreco() {
        return preco;
    }

    public void setPreco(Double preco) {
        this.preco = preco;
    }

    public static ProdutoDTO convertToProdutoDTO(Produto produto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(produto, ProdutoDTO.class);
    }
}
