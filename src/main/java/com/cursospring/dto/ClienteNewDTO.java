package com.cursospring.dto;

import com.cursospring.domain.Cliente;
import com.cursospring.services.validation.ClienteInsert;
import org.hibernate.validator.constraints.Length;
import org.modelmapper.ModelMapper;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.util.List;


@ClienteInsert
public class ClienteNewDTO {

    @NotEmpty(message = "Preenchimento obrigatorio")
    @Length(message = "Minimo  5, Maximo 120",min = 5,max = 120)
    private String nome;

    @NotEmpty(message = "Email Obrigatorio")
    @Email(message = "Email Invalido")
    private String email;

    @NotEmpty(message = "Cpf obrigatorio")
    private String cpfCnpj;

    private Integer tipoCliente;

    @NotEmpty(message = "Endereco obrigatorio")
    private List<EnderecoDTO> enderecos;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getTipoCliente() {
        return tipoCliente;
    }

    public void setTipoCliente(Integer tipoCliente) {
        this.tipoCliente = tipoCliente;
    }

    public List<EnderecoDTO> getEnderecos() {
        return enderecos;
    }

    public void setEnderecos(List<EnderecoDTO> enderecos) {
        this.enderecos = enderecos;
    }

    public String getCpfCnpj() {
        return cpfCnpj;
    }

    public void setCpfCnpj(String cpfCnpj) {
        this.cpfCnpj = cpfCnpj;
    }

    public Cliente convertToCliente(){
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(this, Cliente.class);

    }


}
