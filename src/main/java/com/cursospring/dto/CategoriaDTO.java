package com.cursospring.dto;

import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import com.cursospring.domain.Categoria;
import org.modelmapper.ModelMapper;

import java.io.Serializable;

public class CategoriaDTO implements Serializable {
	
	private Integer id;
	
	@NotEmpty(message="Campo não pode estar vazio")
	@Length(min=5,max=80,message="O tamanho deve ser entre 5 a 80 caracteres")
	private String nome;
	
	public CategoriaDTO() {
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public static Categoria convertToCategoria(CategoriaDTO categoriaDTO) {
		ModelMapper modelMapper = new ModelMapper();
		return modelMapper.map(categoriaDTO, Categoria.class);
	}

	public static CategoriaDTO convertToCategoriaDTO(Categoria categoria) {
		ModelMapper modelMapper = new ModelMapper();
		return modelMapper.map(categoria, CategoriaDTO.class);
	}
	
}
