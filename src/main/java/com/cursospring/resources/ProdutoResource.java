package com.cursospring.resources;

import com.cursospring.domain.Categoria;
import com.cursospring.domain.Pedido;
import com.cursospring.domain.Produto;
import com.cursospring.dto.CategoriaDTO;
import com.cursospring.dto.ProdutoDTO;
import com.cursospring.resources.utils.URL;
import com.cursospring.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/produtos")
public class ProdutoResource {

    @Autowired
    private ProdutoService produtoService;

    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    public ResponseEntity<Produto> find(@PathVariable Integer id){
        return ResponseEntity.ok().body(produtoService.findByid(id));
    }

    @RequestMapping(method=RequestMethod.GET)
    public ResponseEntity<Page<ProdutoDTO>> findPage(
            @RequestParam(value="page", defaultValue="0") Integer page,
            @RequestParam(value="linesPerPage", defaultValue="24") Integer linesPerPage,
            @RequestParam(value="orderBy", defaultValue="nome") String orderBy,
            @RequestParam(value="direction", defaultValue="ASC") String direction,
            @RequestParam(value="nome", defaultValue="") String nome,
            @RequestParam(value="categorias", defaultValue="") String categorias) {

        Page<Produto> produtos = produtoService.search(URL.decodeParam(nome), URL.decodeIntList(categorias),page,linesPerPage,orderBy,direction);
        Page<ProdutoDTO> produtoDTOs = produtos.map(it-> ProdutoDTO.convertToProdutoDTO(it));

        return ResponseEntity.ok().body(produtoDTOs);
    }
}
