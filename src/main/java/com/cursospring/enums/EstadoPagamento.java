package com.cursospring.enums;

public enum EstadoPagamento {

	PENDENTE(0,"Pendente"),
	QUITADO(1,"Quitado"),
	CANCELADO(2,"Cancelado");
	
	private Integer codigo;
	private String descricao;
	
	private EstadoPagamento(Integer codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}
	
	public static EstadoPagamento getEstadoPagamento(Integer codigo) {
		
		if(codigo == null) {
			return null;
		}
		
		for (EstadoPagamento estado : EstadoPagamento.values()) {
			if(codigo.equals(estado.getCodigo())){
				return estado;
			}
		}
		
		throw new IllegalArgumentException("Codigo " + codigo + " inválido");
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	
	
	}


