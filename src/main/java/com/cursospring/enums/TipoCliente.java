package com.cursospring.enums;

public enum TipoCliente {
	
	PESSOA_FISICA(0,"Pessoa Física"),
	PESSOA_JURIDICA(1,"Pessoa Jurídica");
	
	public Integer codigo;
	public String descricao;
	

	 TipoCliente(Integer codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}
	
	public static TipoCliente getTipoPessoa(Integer codigo){
		if(codigo == null) {
			return null;
		}
		
		for (TipoCliente tipo : TipoCliente.values()) {
			if(tipo.getCodigo().equals(codigo)) {
				return tipo;
			}
		}
		throw new IllegalArgumentException("Codigo " + codigo + " inválido");
	}
	
	public Integer getCodigo() {
		return codigo;
	}
	
	public String getDescricao() {
		return descricao;
	}

}
