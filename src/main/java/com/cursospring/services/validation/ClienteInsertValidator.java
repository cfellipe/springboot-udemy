package com.cursospring.services.validation;

import com.cursospring.dto.ClienteNewDTO;
import com.cursospring.enums.TipoCliente;
import com.cursospring.repositories.ClienteRepository;
import com.cursospring.resources.exception.FieldMessage;
import com.cursospring.services.validation.utils.BR;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;

public class ClienteInsertValidator implements ConstraintValidator<ClienteInsert, ClienteNewDTO> {


        @Autowired
        private ClienteRepository clienteRepository;

        @Override
        public void initialize(ClienteInsert ann) {

        }
        @Override
        public boolean isValid(ClienteNewDTO objDto, ConstraintValidatorContext context) {

            List<FieldMessage> list = new ArrayList<>();

            if(objDto.getTipoCliente().equals(TipoCliente.PESSOA_FISICA.codigo) && !BR.isValidCpf(objDto.getCpfCnpj())) {

                list.add(new FieldMessage("CPF","Cpf invalido"));

            } else if(objDto.getTipoCliente().equals(TipoCliente.PESSOA_JURIDICA.codigo) && !BR.isValidCnpj(objDto.getCpfCnpj())) {

                list.add(new FieldMessage("CNPJ","Cnpj Invalido"));
            }

            if(clienteRepository.findByEmail(objDto.getEmail())!=null){
                list.add(new FieldMessage("Email","Email ja existente"));
            }

            // inclua os testes aqui, inserindo erros na lista
            for (FieldMessage e : list) {
                context.disableDefaultConstraintViolation();
                context.buildConstraintViolationWithTemplate(e.getMessage()).addPropertyNode(e.getFieldName()).addConstraintViolation();
            }
            return list.isEmpty();
        }
}

