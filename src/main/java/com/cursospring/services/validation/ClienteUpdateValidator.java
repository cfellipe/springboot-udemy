package com.cursospring.services.validation;

import com.cursospring.domain.Cliente;
import com.cursospring.dto.ClienteDTO;
import com.cursospring.dto.ClienteNewDTO;
import com.cursospring.enums.TipoCliente;
import com.cursospring.repositories.ClienteRepository;
import com.cursospring.resources.exception.FieldMessage;
import com.cursospring.services.validation.utils.BR;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerMapping;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ClienteUpdateValidator implements ConstraintValidator<ClienteUpdate, ClienteDTO> {


        @Autowired
        private ClienteRepository clienteRepository;

        @Autowired
        private HttpServletRequest request;

        @Override
        public void initialize(ClienteUpdate ann) {

        }
        @Override
        public boolean isValid(ClienteDTO objDto, ConstraintValidatorContext context) {

            List<FieldMessage> list = new ArrayList<>();
            Map<String , String> map = (Map<String, String>) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
            Integer urlId = Integer.parseInt(map.get("id"));


            Cliente cliente = clienteRepository.findByEmail(objDto.getEmail());
            if(cliente!=null && !cliente.getId().equals(urlId)){
                list.add(new FieldMessage("Email","Email ja existente"));
            }

            // inclua os testes aqui, inserindo erros na lista
            for (FieldMessage e : list) {
                context.disableDefaultConstraintViolation();
                context.buildConstraintViolationWithTemplate(e.getMessage()).addPropertyNode(e.getFieldName()).addConstraintViolation();
            }
            return list.isEmpty();
        }
}

