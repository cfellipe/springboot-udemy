package com.cursospring.services;

import com.cursospring.domain.PagamentoComBoleto;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;

@Service
public class BoletoService {


    public void preenchePagamentoComBoleto(PagamentoComBoleto pagamentoBoleto, Date instanteDoPedido) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(instanteDoPedido);
        calendar.add(Calendar.DAY_OF_MONTH,7);
        pagamentoBoleto.setDataVencimento(calendar.getTime());
    }
}
