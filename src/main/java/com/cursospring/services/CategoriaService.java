package com.cursospring.services;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.cursospring.domain.Categoria;
import com.cursospring.dto.CategoriaDTO;
import com.cursospring.repositories.CategoriaRepository;
import com.cursospring.services.exceptions.ObjectNotFoundException;

@Service
public class CategoriaService {
	
	@Autowired
	private CategoriaRepository categoriaRepository;
	
	public Categoria find(Integer id){
		Optional<Categoria> categoria = categoriaRepository.findById(id);
		return categoria.orElseThrow(()-> new ObjectNotFoundException("Objeto não encontrado! Id: " +id + ", Tipo: " + Categoria.class.getName()));
		
	}

	public Categoria insert(Categoria categoria) {
		return categoriaRepository.save(categoria);
	}

	public Categoria update(CategoriaDTO categoriaDTO, Integer id) {
		Categoria categoriaFind = this.find(id);
		categoriaFind.setNome(categoriaDTO.getNome());

		return categoriaRepository.save(categoriaFind);
	}

	public void delete(Integer id) {
		this.find(id);
		try {
			categoriaRepository.deleteById(id);
		}catch(DataIntegrityViolationException e){
			throw new DataIntegrityViolationException("Não é possivel deletar uma categoria que possui produtos");
		}
	}

	public List<Categoria> findAll() {
		return categoriaRepository.findAll();
	}
	
	public Page<Categoria> findPage(Integer page, Integer linesPerPage, String orderBy, String direction){
		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
		
		return categoriaRepository.findAll(pageRequest);
	}
	




}
