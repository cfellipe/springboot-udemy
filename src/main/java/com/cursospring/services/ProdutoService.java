package com.cursospring.services;

import com.cursospring.domain.Categoria;
import com.cursospring.domain.Produto;
import com.cursospring.repositories.CategoriaRepository;
import com.cursospring.repositories.ProdutoRepository;
import com.cursospring.services.exceptions.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProdutoService {

    @Autowired
    private ProdutoRepository produtoRepository;

    @Autowired
    private CategoriaRepository categoriaRepository;

    public Produto findByid(Integer id){
        Optional<Produto> optionalProduto = produtoRepository.findById(id);
        return optionalProduto.orElseThrow(()-> new ObjectNotFoundException("Objeto não encontrado! Id: " +id + ", Tipo: " + Produto.class.getName()));
    }

    public Page<Produto> search(String nome, List<Integer> id, Integer page, Integer linesPerPage, String orderBy, String direction){
        PageRequest pageRequest = PageRequest.of(page, linesPerPage, Sort.Direction.valueOf(direction), orderBy);
        List<Categoria> categorias = categoriaRepository.findAllById(id);

        return produtoRepository.findDistinctByNomeContainingAndCategoriasIn(nome,categorias,pageRequest);

    }
}
