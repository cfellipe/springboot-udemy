package com.cursospring.services;

import java.util.*;

import com.cursospring.domain.Cidade;
import com.cursospring.dto.ClienteDTO;
import com.cursospring.dto.ClienteNewDTO;
import com.cursospring.enums.TipoCliente;
import com.cursospring.repositories.CidadeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import com.cursospring.domain.Cliente;
import com.cursospring.repositories.ClienteRepository;
import com.cursospring.services.exceptions.ObjectNotFoundException;
import org.springframework.util.CollectionUtils;

@Service
public class ClienteService {
	
	@Autowired
	private ClienteRepository clienteRepository;

	@Autowired
	private CidadeRepository cidadeRepository;
	
	
	public Cliente find(Integer id) {
		Optional<Cliente> cliente = clienteRepository.findById(id);
		return cliente.orElseThrow(()-> new ObjectNotFoundException("Objeto não encontrado! Id: " +id + ", Tipo: " + Cliente.class.getName()));
	}

	public List<Cliente> findAll() {
		return clienteRepository.findAll();
	}

	public void update(Integer id, ClienteDTO clienteDTO) {
		Cliente clienteFind = this.find(id);
		clienteFind.setNome(clienteDTO.getNome());
		clienteFind.setEmail(clienteDTO.getEmail());
		clienteRepository.save(clienteFind);
	}

	public void delete(Integer id){
		this.find(id);
		try {
			clienteRepository.deleteById(id);
		}catch (DataIntegrityViolationException e) {
			throw new DataIntegrityViolationException("Não é possivel deletar um cliente que possui pedidos relacionados");
		}
	}

	public Page<Cliente> findPage(Integer page, Integer linesPerPage, String orderBy, String direction){
		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Sort.Direction.valueOf(direction), orderBy);

		return clienteRepository.findAll(pageRequest);
	}

    public Cliente insert(ClienteNewDTO newCliente) {
		Cliente cliente = newCliente.convertToCliente();
		cliente.setTipoCliente(TipoCliente.getTipoPessoa(newCliente.getTipoCliente()));

		if (!CollectionUtils.isEmpty(newCliente.getEnderecos())) {
			for (int i = 0; i < newCliente.getEnderecos().size(); i++) {
				Optional<Cidade> cidadeOpt = cidadeRepository.findById(newCliente.getEnderecos().get(i).getCidadeId());
				cliente.getEnderecos().get(i).setCidade(cidadeOpt.orElse(null));
				cliente.getEnderecos().get(i).setCliente(cliente);
			}
		}

		return clienteRepository.save(cliente);
    }
}
