package com.cursospring.services;

import java.util.Date;
import java.util.Optional;

import com.cursospring.domain.ItemPedido;
import com.cursospring.domain.PagamentoComBoleto;
import com.cursospring.domain.Produto;
import com.cursospring.enums.EstadoPagamento;
import com.cursospring.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cursospring.domain.Pedido;
import com.cursospring.repositories.PedidoRepository;
import com.cursospring.services.exceptions.ObjectNotFoundException;

@Service
public class PedidoService {

	@Autowired
	private PedidoRepository pedidoRepository;

	@Autowired
	private ProdutoRepository produtoRepository;

	@Autowired
	private BoletoService boletoService;
	
	public Pedido find(Integer id) {
		Optional<Pedido> pedidoOptional = pedidoRepository.findById(id);
		return pedidoOptional.orElseThrow(()-> new ObjectNotFoundException("Objeto não encontrado com Id: "+ id +" Tipo: "+ Pedido.class.getName()));
	}

	public Pedido insert(Pedido obj) {
		obj.setId(null);
		obj.setInstante(new Date());
		obj.getPagamento().setEstado(EstadoPagamento.PENDENTE);
		obj.getPagamento().setPedido(obj);
		if (obj.getPagamento() instanceof PagamentoComBoleto){
			PagamentoComBoleto pagamentoBoleto = (PagamentoComBoleto) obj.getPagamento();
			boletoService.preenchePagamentoComBoleto(pagamentoBoleto,obj.getInstante());
		}
		for (ItemPedido ip:obj.getItens()) {
			ip.setDesconto(0.0);
			ip.setPreco(produtoRepository.findById(ip.getProduto().getId()).get().getPreco());
			ip.setPedido(obj);
		}

		return obj = pedidoRepository.save(obj);
	}
}
