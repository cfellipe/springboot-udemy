package com.cursospring;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;

import javax.persistence.EnumType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.cursospring.domain.Categoria;
import com.cursospring.domain.Cidade;
import com.cursospring.domain.Cliente;
import com.cursospring.domain.Endereco;
import com.cursospring.domain.Estado;
import com.cursospring.domain.ItemPedido;
import com.cursospring.domain.Pagamento;
import com.cursospring.domain.PagamentoComBoleto;
import com.cursospring.domain.PagamentoComCartao;
import com.cursospring.domain.Pedido;
import com.cursospring.domain.Produto;
import com.cursospring.enums.EstadoPagamento;
import com.cursospring.enums.TipoCliente;
import com.cursospring.repositories.CategoriaRepository;
import com.cursospring.repositories.CidadeRepository;
import com.cursospring.repositories.ClienteRepository;
import com.cursospring.repositories.EnderecoRepository;
import com.cursospring.repositories.EstadoRepository;
import com.cursospring.repositories.ItemPedidoRepository;
import com.cursospring.repositories.PagamentoRepository;
import com.cursospring.repositories.PedidoRepository;
import com.cursospring.repositories.ProdutoRepository;
import com.cursospring.resources.CategoriaResource;

@SpringBootApplication
public class CursoSpringApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(CursoSpringApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {


	}

}

